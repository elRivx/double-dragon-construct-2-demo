# Double Dragon Construct 2 Demo

## Contenidos

- [Información](#Información)
- [Objetivos](#Objetivos)

## Información

Éste proyecto es un ejemplo del uso de Construct 2 como motor de videojuegos con un ejemplo de Beat'em Up.


## Objetivos

- [x] Crear Archivos principales nivel y personaje
- [x] Movimientos básicos del personaje
- [ ] Mejorar Diseño

